<?php

    $cfg['banner'] = [
        'default_language' => 'RU',                     // fyfywkaBanner/lang
        'time_zone'        => 'Europe/Kiev',            //
        'img'              => 'img1.png',               // fyfywkaBanner/img
        'filter'           => 'img0.png',               // fyfywkaBanner/img
        'font'             => 'Raleway-Medium.ttf',     // fyfywkaBanner/font
    ];